<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\FaqController;
use App\Http\Controllers\Api\CollectionController;
use App\Http\Controllers\Api\OrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('users', 'App\Http\Controllers\Api\UserController');
Route::post('users/promo/{id}', [UserController::class, 'createUserPromoCode']);
Route::get('users/promo/{id}', [UserController::class, 'getUserPromoCode']);
Route::post('users/promo/redeem/{id}', [UserController::class, 'redeemUserPromoCode']);
Route::post('users/promo/delete/{id}', [UserController::class, 'destroyUserPromoCode']);
Route::resource('admin', 'App\Http\Controllers\Api\AdminController');

Route::get('products/get', [ProductController::class, 'index']);
Route::post('products/store', [ProductController::class, 'store']);
Route::put('products/edit/{id}', [ProductController::class, 'edit']);
Route::put('products/update/{id}', [ProductController::class, 'update']);
Route::delete('products/delete/{id}', [ProductController::class, 'destroy']);

Route::post('faq/post',[FaqController::class,'store']);
Route::get('faq/get',[FaqController::class,'index']);
Route::put('faq/{id}', [FaqController::class, 'edit']);
Route::delete('faq/{id}', [FaqController::class, 'destroy']);

Route::get('collections/get', [CollectionController::class, 'index']);
Route::post('collections/store', [CollectionController::class, 'store']);
Route::post('collections/add-products/{id}', [CollectionController::class, 'addProducts']);
Route::put('collections/edit/{id}', [CollectionController::class, 'edit']);
Route::delete('collections/delete/{id}', [CollectionController::class, 'destroy']);
Route::post('collections/delete-product', [CollectionController::class, 'destroyProduct']);

Route::get('orders/get', [OrderController::class, 'index']);
Route::post('orders/store', [OrderController::class, 'store']);
Route::get('orders/show/{id}', [OrderController::class, 'show']);
Route::put('orders/update/{id}', [OrderController::class, 'update']);
Route::delete('orders/delete/{id}', [OrderController::class, 'destroy']);
